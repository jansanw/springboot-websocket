package cn.lili.controller.tenant;

import cn.lili.modules.seat.dos.SeatSetting;
import cn.lili.modules.seat.service.SeatSettingService;
import cn.lili.security.UserContext;
import cn.lili.result.ResultMessage;
import cn.lili.result.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * 店铺端,分类绑定参数组管理接口
 *
 * @author pikachu
 * @since 2020-02-18 15:18:56
 */
@RestController
@Api(tags = "店铺端,坐席设置")
@RequestMapping("/store/seat/setting")
@Transactional(rollbackFor = Exception.class)
public class SeatSettingStoreController {

    @Autowired
    private SeatSettingService seatSettingService;

    @ApiOperation(value = "查询坐席设置")
    @GetMapping
    public ResultMessage<SeatSetting> getSetting() {
        return ResultUtil.data(seatSettingService.getSetting(UserContext.getCurrentUser().getTenantId()));
    }

    @ApiOperation(value = "更新坐席设置")
    @PutMapping
    public void update(SeatSetting seatSetting) {
        seatSetting.setTenantId(UserContext.getCurrentUser().getTenantId());
        seatSettingService.updateByStore(seatSetting);
    }
}
