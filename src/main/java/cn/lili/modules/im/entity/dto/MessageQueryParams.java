package cn.lili.modules.im.entity.dto;

import cn.lili.exception.ServiceException;
import cn.lili.modules.im.entity.ImMessage;
import cn.lili.result.ResultCode;
import cn.lili.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;

/**
 * MessageQueryParams
 *
 * @author Chopper
 * @version v1.0
 * 2022-01-20 17:16
 */
@Data
public class MessageQueryParams {
    /**
     * 聊天窗口
     */
    private String talkId;
    /**
     * 最后一个消息
     */
    private String lastMessageId;
    /**
     * 获取消息数量
     */
    private Integer num;

    public LambdaQueryWrapper<ImMessage> initQueryWrapper() {
        if (StringUtils.isEmpty(talkId)) {
            throw new ServiceException(ResultCode.ERROR);
        }
        if (num == null || num > 50) {
            num = 50;
        }

        LambdaQueryWrapper<ImMessage> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ImMessage::getTalkId, talkId);
        if (StringUtils.isNotEmpty(lastMessageId)) {
            lambdaQueryWrapper.lt(ImMessage::getId, lastMessageId);
        }
        lambdaQueryWrapper.orderByDesc(ImMessage::getId);
        lambdaQueryWrapper.last("limit " + num);
        return lambdaQueryWrapper;
    }
}
