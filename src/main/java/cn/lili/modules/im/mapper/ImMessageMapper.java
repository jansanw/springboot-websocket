package cn.lili.modules.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.im.entity.ImMessage;

import java.util.List;

/**
 * Im消息 Dao层
 * @author Chopper
 */
public interface ImMessageMapper extends BaseMapper<ImMessage> {

}