package cn.lili.modules.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.lili.modules.im.entity.ImUser;

/**
 * Im消息 业务层
 *
 * @author Chopper
 */
public interface ImUserService extends IService<ImUser> {

    /**
     * 注册用户
     *
     * @param accessToken
     * @return
     */
    ImUser register(String accessToken);

}