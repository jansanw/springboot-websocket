package cn.lili.modules.seat.handler;

import cn.lili.modules.seat.dos.Seat;
import cn.lili.modules.seat.service.SeatService;
import cn.lili.security.AuthUser;
import cn.lili.security.enums.UserEnums;
import cn.lili.security.token.Token;
import cn.lili.security.token.TokenUtil;
import cn.lili.security.token.base.AbstractTokenGenerate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员token生成
 *
 * @author Chopper
 * @version v4.0
 * @since 2020/11/16 10:50
 */
@Component
public class SeatTokenGenerate extends AbstractTokenGenerate<Seat> {
    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public Token createToken(Seat seat, Boolean longTerm) {
        AuthUser authUser = new AuthUser(
                seat.getUsername(),
                seat.getId(),
                seat.getNickName(),
                seat.getFace(),
                UserEnums.SEAT);
        authUser.setTenantId(seat.getTenantId());
        //登陆成功生成token
        return tokenUtil.createToken(seat.getUsername(), authUser, longTerm, UserEnums.SEAT);
    }

    @Override
    public Token refreshToken(String refreshToken) {
        return tokenUtil.refreshToken(refreshToken, UserEnums.SEAT);
    }

}
