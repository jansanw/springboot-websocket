package cn.lili.modules.tenant.controller;

import cn.lili.modules.tenant.entity.TenantUser;
import cn.lili.modules.tenant.service.TenantUserService;
import cn.lili.mybatis.util.PageUtil;
import cn.lili.result.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author Chopper
 */
@RestController
@Api(tags = "租户用户接口")
@RequestMapping("/lili/tenantUser")
public class TenantUserController {

    @Autowired
    private TenantUserService tenantUserService;

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查看租户用户详情")
    public ResultMessage<TenantUser> get(@PathVariable String id){

        TenantUser tenantUser = tenantUserService.getById(id);
        return new ResultUtil<TenantUser>().setData(tenantUser);
    }

    @GetMapping
    @ApiOperation(value = "分页获取租户用户")
    public ResultMessage<IPage<TenantUser>> getByPage(TenantUser entity,
                                                        SearchVO searchVo,
                                                        PageVO page){
        IPage<TenantUser> data = tenantUserService.page(PageUtil.initPage(page),PageUtil.initWrapper(entity, searchVo));
        return new ResultUtil<IPage<TenantUser>>().setData(data);
    }

    @PostMapping
    @ApiOperation(value = "新增租户用户")
    public ResultMessage<TenantUser> save(TenantUser tenantUser){

        if(tenantUserService.save(tenantUser)){
            return new ResultUtil<TenantUser>().setData(tenantUser);
        }
        return new ResultUtil<TenantUser>().setErrorMsg(ResultCode.ERROR);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "更新租户用户")
    public ResultMessage<TenantUser> update(@PathVariable String id, TenantUser tenantUser){
        if(tenantUserService.updateById(tenantUser)){
            return new ResultUtil<TenantUser>().setData(tenantUser);
        }
        return new ResultUtil<TenantUser>().setErrorMsg(ResultCode.ERROR);
    }

    @DeleteMapping(value = "/{ids}")
    @ApiOperation(value = "删除租户用户")
    public ResultMessage<Object> delAllByIds(@PathVariable List ids){

        tenantUserService.removeByIds(ids);
        return ResultUtil.success();
    }
}
