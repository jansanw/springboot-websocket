package cn.lili.modules.tenant.entity;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 租户
 *
 * @author Chopper
 * @version v1.0
 * 2022-02-14 15:56
 */
@Data
@TableName("li_tenant")
@ApiModel(value = "租户")
@NoArgsConstructor
public class Tenant extends BaseEntity {

    @ApiModelProperty(value = "租户名称")
    private String tenantName;

    @ApiModelProperty(value = "租户logo")
    private String tenantLogo;

    @ApiModelProperty(value = "accessKey")
    private String accessKey;

    @ApiModelProperty(value = "accessKeySecret")
    private String accessKeySecret;

    @ApiModelProperty(value = "失效时间")
    private Date invalidTime;

}
