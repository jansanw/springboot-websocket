package cn.lili.modules.tenant.entity;

import cn.lili.mybatis.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * 租户用户
 *
 * @author Chopper
 * @version v1.0
 * 2022-02-14 15:56
 */
@Data
@TableName("li_tenant")
@ApiModel(value = "租户")
@NoArgsConstructor
public class TenantUser extends BaseEntity {

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "用户名")
    @Length(max = 20, message = "用户名长度不能超过20个字符")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;


}
