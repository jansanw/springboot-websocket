package cn.lili.modules.tenant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.tenant.entity.Tenant;

import java.util.List;

/**
 * 租户 Dao层
 * @author Chopper
 */
public interface TenantMapper extends BaseMapper<Tenant> {

}