package cn.lili.modules.tenant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lili.modules.tenant.entity.TenantUser;

import java.util.List;

/**
 * 租户用户 Dao层
 * @author Chopper
 */
public interface TenantUserMapper extends BaseMapper<TenantUser> {

}