package cn.lili.modules.tenant.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.lili.modules.tenant.entity.TenantUser;

import java.util.List;

/**
 * 租户用户 业务层
 * @author Chopper
 */
public interface TenantUserService extends IService<TenantUser> {

}