package cn.lili.modules.tenant.serviceimpl;

import cn.lili.modules.tenant.mapper.TenantMapper;
import cn.lili.modules.tenant.entity.Tenant;
import cn.lili.modules.tenant.service.TenantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 租户 业务实现
 * @author Chopper
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements TenantService {

    @Autowired
    private TenantMapper tenantMapper;
}