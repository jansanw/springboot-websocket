package cn.lili.modules.tenant.serviceimpl;

import cn.lili.modules.tenant.mapper.TenantUserMapper;
import cn.lili.modules.tenant.entity.TenantUser;
import cn.lili.modules.tenant.service.TenantUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 租户用户 业务实现
 * @author Chopper
 */
@Service
public class TenantUserServiceImpl extends ServiceImpl<TenantUserMapper, TenantUser> implements TenantUserService {

    @Autowired
    private TenantUserMapper tenantUserMapper;
}