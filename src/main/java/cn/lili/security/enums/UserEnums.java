package cn.lili.security.enums;

/**
 * token角色类型
 *
 * @author Chopper
 * @version v1.0
 * @since 2020/8/18 15:23
 */
public enum UserEnums {
    /**
     * 角色
     */
    MANAGER("管理员"),
    MEMBER("会员"),
    SELLER("租户"),
    TENANT("租户"),
    SEAT("坐席");

    private final String role;

    UserEnums(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
