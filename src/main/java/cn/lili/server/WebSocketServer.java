package cn.lili.server;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import cn.lili.config.CustomSpringConfigurator;
import cn.lili.cache.Cache;
import cn.lili.modules.im.entity.ImMessage;
import cn.lili.modules.im.entity.ImUser;
import cn.lili.modules.im.entity.enums.MessageResultType;
import cn.lili.modules.im.entity.vo.MessageVO;
import cn.lili.modules.im.service.ImMessageService;
import cn.lili.modules.im.service.ImUserService;
import cn.lili.security.AuthUser;
import cn.lili.security.UserContext;
import cn.lili.security.enums.UserEnums;
import cn.lili.utils.SnowFlake;
import com.alibaba.druid.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSON;
import cn.lili.modules.im.entity.vo.MessageOperation;

/**
 * @author liushuai
 */
@Component
@ServerEndpoint(value = "/lili/webSocket/{accessToken}", configurator = CustomSpringConfigurator.class)
@Slf4j
public class WebSocketServer {
    /**
     * 消息服务
     */
    @Autowired
    private ImMessageService imMessageService;

    /**
     * im用户服务
     */
    @Autowired
    private ImUserService imUserService;


    @Autowired
    private Cache cache;
    /**
     * 在线人数
     * PS 注意，只能单节点，如果多节点部署需要自行寻找方案
     */
    private static ConcurrentHashMap<String, Session> sessionPools = new ConcurrentHashMap<>();

    /**
     * 建立连接
     *
     * @param session
     */
    @OnOpen
    public void onOpen(@PathParam("accessToken") String accessToken, Session session) throws IOException {

        ImUser imUser = imUserService.register(accessToken);
        AuthUser authUser = UserContext.getAuthUser(accessToken);
        sessionPools.put(authUser.getId(), session);
        MessageVO messageVO = new MessageVO(MessageResultType.FRIENDS, imUser);
        sendMessage(authUser.getId(), messageVO);
    }

    /**
     * 关闭连接
     */
    @OnClose
    public void onClose(@PathParam("accessToken") String accessToken) {
        log.info("断开连接:{}", accessToken);
        sessionPools.remove(UserContext.getAuthUser(accessToken).getId());
    }

    /**
     * 发送消息
     *
     * @param msg
     * @throws IOException
     */
    @OnMessage
    public void onMessage(@PathParam("accessToken") String accessToken, String msg) {
        log.error(msg);
        MessageOperation messageOperation = JSON.parseObject(msg, MessageOperation.class);
        operation(accessToken, messageOperation);
    }

    /**
     * IM操作
     *
     * @param accessToken
     * @param messageOperation
     */
    private void operation(String accessToken, MessageOperation messageOperation) {

        AuthUser authUser = UserContext.getAuthUser(accessToken);
        switch (messageOperation.getOperationType()) {
            case PING:
                break;
            case MESSAGE:
                //保存消息
                ImMessage imMessage = new ImMessage();
                imMessage.setFromUser(authUser.getId());
                imMessage.setMessageType(messageOperation.getMessageType());
                imMessage.setIsRead(false);
                imMessage.setText(messageOperation.getContext());
                imMessage.setTalkId(messageOperation.getTalkId());
                imMessage.setCreateTime(new Date());
                imMessage.setToUser(messageOperation.getTo());
                imMessage.setId(SnowFlake.getIdStr());
                imMessageService.save(imMessage);
                //发送消息
                sendMessage(messageOperation.getTo(), new MessageVO(MessageResultType.MESSAGE, imMessage));
                break;
            case READ:
                if (!StringUtils.isEmpty(messageOperation.getContext())) {
                    imMessageService.read(messageOperation.getTalkId(), accessToken);
                }
                break;
            case UNREAD:
                sendMessage(authUser.getId(), new MessageVO(MessageResultType.UN_READ, imMessageService.unReadMessages(accessToken)));
                break;
            case HISTORY:
                sendMessage(authUser.getId(), new MessageVO(MessageResultType.HISTORY, imMessageService.historyMessage(accessToken, messageOperation.getTo())));
                break;
            default:
                break;
        }
    }

    /**
     * 发送消息
     *
     * @param key     密钥
     * @param message 消息对象
     */
    private void sendMessage(String key, MessageVO message) {
        Session session = sessionPools.get(key);
        if (session != null) {
            try {
                session.getBasicRemote().sendText(JSON.toJSONString(message, true));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * socket exception
     *
     * @param session
     * @param throwable
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        throwable.printStackTrace();
    }

    /**
     * 获取店铺id
     *
     * @return
     */
    private String storeKey(String storeId) {
        return "STORE_" + storeId;
    }

}
